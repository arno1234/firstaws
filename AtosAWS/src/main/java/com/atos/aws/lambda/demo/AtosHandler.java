package com.atos.aws.lambda.demo;

/**
 * Test AWS Lambda. 
 * Java Handler
 * 
 */

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class AtosHandler implements RequestHandler<Object, String> {

    @Override
    public String handleRequest(Object input, Context context) {

        context.getLogger().log("Input: " + input);
        String output = "ATOS says Hello from BEZONS to Mr " + input + " ! - PUBLIC S3";
        //return output;
        
        String reponse = "{ response = " + output + " }";
        return  reponse ;
    }

}
